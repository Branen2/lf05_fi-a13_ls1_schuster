package b2;
import java.util.Scanner; // Import der Klasse Scanner
public class scanneruebung
{

	public static void main(String[] args) // Hier startet das Programm
 {

 // Neues Scanner-Objekt myScanner wird erstellt
		Scanner myScanner = new Scanner(System.in);
	
		System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

 // Die Variable zahl1 speichert die erste Eingabe
		int zahl1 = myScanner.nextInt();

		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

 // Die Variable zahl2 speichert die zweite Eingabe
		int zahl2 = myScanner.nextInt();

 // Die Addition der Variablen zahl1 und zahl2
 // wird der Variable ergebnis zugewiesen.
		int ergebnisaddition = zahl1 + zahl2;
		int ergebnissubtraktion = zahl1 - zahl2;
		int ergebnismultiplikation = zahl1 * zahl2;
		int ergebnisdivision = zahl1 / zahl2;

		System.out.print("\n\n\nErgebnis der Addition lautet: ");
		System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnisaddition);
		
		System.out.print("\n\n\nErgebnis der Subtraktion lautet: ");
		System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnissubtraktion);
		
		System.out.print("\n\n\nErgebnis der Multiplikation lautet: ");
		System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnismultiplikation);
		
		System.out.print("\n\n\nErgebnis der Divison lautet: ");
		System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnisdivision);
		System.out.print("\nmit einem Rest von" + (zahl1%zahl2));
		myScanner.close();

 }
}