package b2;

import java.util.Scanner;

public class roemmischeZahlen {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("R�mische Jahreszahl eingeben:");
		String jahreszahl = scanner.next();

		int laenge = jahreszahl.length();
		int ergebnis = 0;
		
		System.out.println(umwandeln(jahreszahl, laenge, ergebnis));
		
		scanner.close();
	}
	
	public static int romZuDezi(char character) {
			switch(character) {
			case 'I':
				return 1;
			case 'V':
				return 5;
			case 'X':
				return 10;
			case 'L':
				return 50;
			case 'C':
				return 100;
			case 'D':
				return 500;
			case 'M':
				return 1000;
			default:
				System.out.println("nicht genemigte Eingabe");
				return 0;
			}
	}
		
	public static int umwandeln(String rom, int laenge, int ergebnis) {
		for(int i = 0; i < laenge; i++) {
			try {
				if(romZuDezi(rom.charAt(i))>=(romZuDezi(rom.charAt(i+1)))) {
					ergebnis = ergebnis+romZuDezi(rom.charAt(i));
				} else {
					ergebnis = ergebnis+(romZuDezi(rom.charAt(i+1))-romZuDezi(rom.charAt(i)));
					i++;
				}
			} catch(Exception e) {
				ergebnis = ergebnis+romZuDezi(rom.charAt(i));
			}
		} 			
		return ergebnis;
	}
}