
public class Temperaturtabelle {

	public static void main(String[] args) {
		String fahrenheit = "Fahrenheit";
		String celsius = "Celsius";
		int zero 	= 0;
		int ten 	= 10;
		int twenty 	= 20;
		int thirty 	= 30;
		double d1 	= -28.8889;
		double d2 	= -23.3333;
		double d3 	= -17.7778;
		double d4 	= -6.6667;
		double d5 	= -1.1111;
		System.out.printf("%-12s|%10s\n", fahrenheit , celsius);
		System.out.printf("%.23s\n", "---------------------------");
		System.out.printf("%+-12d|%10.2f\n", -twenty, d1);
		System.out.printf("%+-12d|%10.2f\n", -ten, d2);
		System.out.printf("%+-12d|%10.2f\n", zero, d3);
		System.out.printf("%+-12d|%10.2f\n", twenty, d4);
		System.out.printf("%+-12d|%10.2f\n", thirty, d5);

	}

}
