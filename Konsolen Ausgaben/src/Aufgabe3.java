
public class Aufgabe3 {

	public static void main(String[] args) {
		
		//vordefinieren der Zahlen als Double
		double z1 = 22.423423;
		double z2 = 111.2222;
		double z3 = 4.0;
		double z4 = 1000000.551;
		double z5 = 97.34;
		
		//Ausgabe der vordefinierten Zahlen; gerundet auf 2 Nachkommastellen
		System.out.printf("|%.2f|\n", z1);
		System.out.printf("|%.2f|\n", z2);
		System.out.printf("|%.2f|\n", z3);
		System.out.printf("|%.2f|\n", z4);
		System.out.printf("|%.2f|\n", z5);

	}

}
