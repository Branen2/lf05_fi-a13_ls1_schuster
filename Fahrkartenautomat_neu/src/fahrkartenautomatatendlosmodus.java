import java.util.Scanner;

public class fahrkartenautomatatendlosmodus {

	public static void main(String[] args)
    {
    	while (true) {
	    	double zuZahlenderBetrag = fahrkartenbestellungErfassen();
	    	double r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	    	fahrkartenAusgeben();
	    	rueckgeldAusgeben(r�ckgabebetrag);
	
	    	System.out.println("\nVergessen Sie nicht, die Fahrscheine\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir w�nschen Ihnen eine gute Fahrt.\n\n"+
	                          "--------------------------------------------------\n\n");
    	}
    }
    
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	
    	String[] bezeichnung = new String[10];
    	double[]  preis = new double[10];
    	double ticketEinzelpreis = 0;
    	int ticket;
    	boolean done = false;
    	
    	bezeichnung[0] = "Einzelfahrschein Berlin AB";
    	bezeichnung[1] = "Einzelfahrschein Berlin BC";
    	bezeichnung[2] = "Einzelfahrschein Berlin ABC";
    	bezeichnung[3] = "Kurzstrecke";
    	bezeichnung[4] = "Tageskarte Berlin AB";
    	bezeichnung[5] = "Tageskarte Berlin AC";
    	bezeichnung[6] = "Tageskarte Berlin BC";
    	bezeichnung[7] = "Kleingruppen-Tageskarte Berlin AB";
    	bezeichnung[8] = "Kleingruppen-Tageskarte Berlin BC";
    	bezeichnung[9] = "Kleingruppen-Tageskarte Berlin ABC";
    	
    	
    	preis[0] = 2.90;
    	preis[1] = 3.30;
    	preis[2] = 3.60;
    	preis[3] = 1.90;
    	preis[4] = 8.60;
    	preis[5] = 9.00;
    	preis[6] = 9.60;
    	preis[7] = 23.50;
    	preis[8] = 24.30;
    	preis[9] = 24.90;

    	// Vorteil: Preis und Bezeichnung sind im nachhinein leichter zu �ndern
    	// Preise und Tickets sind fest und werden nicht manuell vom Nutzer eingegeben
    	
    	System.out.println("W�hlen sie ihre Ticketart");
    	for(int i = 0; i<preis.length; i++) {
    	System.out.printf("%2d %-35s", i+1,bezeichnung[i]);
    	System.out.printf("%10.2f �\n", preis[i]);
    	}
    	
    	do {
    		System.out.println("Nummer:");
    		ticket = tastatur.nextInt();
    	if(ticket >= 0 && ticket<=10) {
    	done = true;
    	}else {
    		System.out.println("ung�ltige eingabe");
    	}
    	} while(!done);
    	ticket= ticket-1;
    	ticketEinzelpreis = preis[ticket];
    	System.out.print("Wie viele Tickets werden gekauft?: ");
        int anzahlTickets = tastatur.nextInt();
            	
    	return ticketEinzelpreis * anzahlTickets;
    }
    
    // durch die Abfrage aus den arrays kann man Zentral Preis und Ticketart �ndern
    // es wird weniger Speicher ben�tigt
    //Nachteil: man muss die arrays komplett vor-definieren // und eine abfrage bauenn, die auf ein array zugreifen kann --> code ist "verschachtelter"
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneM�nze;
        
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   //System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.printf("%s%.2f EURO %s", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag),"  ");
     	   System.out.print("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
    	
    	return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    public static void warte (int time) {
    	try {
  			Thread.sleep(time);
  		} catch (InterruptedException e) {
  			e.printStackTrace();
  		}
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrscheine werden ausgegeben.");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    }
    
    public static void muenzeAusgeben(double r�ckgabebetrag, String einheit) {
    	while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
        {
     	  System.out.println("2 " + einheit);
	          r�ckgabebetrag -= 2.0;
        }
        while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
        {
     	  System.out.println("1 " + einheit);
	          r�ckgabebetrag -= 1.0;
        }
        while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
        {
     	  System.out.println("0.50 " + einheit);
	          r�ckgabebetrag -= 0.5;
        }
        while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
        {
     	  System.out.println("0.20 " + einheit);
	          r�ckgabebetrag -= 0.2;
        }
        while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
        {
     	  System.out.println("0.10 " + einheit);
	          r�ckgabebetrag -= 0.1;
        }
        while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
        {
     	  System.out.println("0.05 " + einheit);
	          r�ckgabebetrag -= 0.05;
        }
    }
    
    public static void rueckgeldAusgeben(double r�ckgabebetrag) {
    	if(r�ckgabebetrag > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO", r�ckgabebetrag);
     	   System.out.println("\nwird in folgenden M�nzen ausgezahlt:");
     	   muenzeAusgeben(r�ckgabebetrag, " EURO");
        }
    }
}

