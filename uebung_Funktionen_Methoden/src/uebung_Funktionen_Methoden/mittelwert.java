package uebung_Funktionen_Methoden;

public class mittelwert {
	public static double berechnung(double x, double y) {
		return (x + y) / 2.0;
	}
	
	
	 public static void main(String[] args) {

	      double x = 2.0;
	      double y = 4.0;
	      
	      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, (berechnung(x, y)));
	   }
}
