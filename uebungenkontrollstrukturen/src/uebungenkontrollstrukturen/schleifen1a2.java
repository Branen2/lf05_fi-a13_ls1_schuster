package uebungenkontrollstrukturen;

import java.util.Scanner;

public class schleifen1a2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("\"n\" festlegen");
		int n = scanner.nextInt();
		
		int acc = 0;
		for(int i = 1; i <= n; i++) {
			acc = acc+i;
		}
		System.out.println("1+2+3+...+n");
		System.out.println(acc);
		
		acc = 0;
		
		for(int i = 2; i <= 2*n; i+=2) {
			acc = acc+i;
		}
		System.out.println("2+4+6+....+2n");
		System.out.println(acc);
		
		acc = 0;
		
		for(int i = 1; i <= 2*n+1; i+=1) {
			acc = acc+i;
		}
		System.out.println("1+3+5+...+2n+1");
		System.out.println(acc);
	}

}
