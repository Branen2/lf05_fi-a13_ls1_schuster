package uebungenkontrollstrukturen;

import java.util.Scanner;

public class chleifen2a1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("\"n\" festlegen");
		int n = scanner.nextInt();
		 
		int x = 0;
		System.out.println("von 1 bis n");
		while(x < n) {
			x++;
			System.out.println(x);
		}
		
		System.out.println("von n bis 1");
		while (n > 1) {
			n--;
			System.out.println(n);
		}
		

	}

}
