package PackageSeminar;

public class Verein {
	
	private String name;
	private final int MAX_MITGLIEDER;
	private Person[] mitglieder;
	private int belegtePlaetze;
	
	// Constructor
	public Verein() {
		this.MAX_MITGLIEDER = 5;
		mitglieder = new Person[MAX_MITGLIEDER];

	}
	
	// weitere Methoden
	public void beitreten(Person person) {
		mitglieder[belegtePlaetze] = person;
		belegtePlaetze++;
	}
	
	public int freiPlatze() {
		return MAX_MITGLIEDER - belegtePlaetze;
	}
}


