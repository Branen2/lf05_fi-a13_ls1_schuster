package PackageSeminar;

public class Anwendung {
	
	public static void main(String[] args) {
		Person bruno = new Person("Bruno", "Schraepler");
		Person myPerson = new Person("my", "Person");
		
		Verein dorfschuetzen = new Verein();
		
		dorfschuetzen.beitreten(bruno);
		dorfschuetzen.beitreten(myPerson);
		
		System.out.println(dorfschuetzen.freiPlatze());
	}
}
