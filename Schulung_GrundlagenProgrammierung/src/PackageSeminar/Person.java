package PackageSeminar;

public class Person {
	// Attribute
	private String vorname;
	private String nachname;
	private int koerpergroesseInCm;
	private char geschlecht;

	private Person partner;
	private Familienstand familienstand;

	// Constructor
	public Person() {

	}

	public Person(String vorname, String nachname) {
		this.vorname = vorname;
		this.nachname = nachname;
		this.familienstand = Familienstand.SINGLE;
	}

	// weitere Methoden
	public String getName() {
		return (this.vorname + " " + this.nachname);
	}

	public void vorstellen() {
		System.out.println("Hallo, mein Name ist " + getName() + ".");
	}
	
	public void heiraten(Person partner) {
		this.partner = partner;
		this.familienstand = Familienstand.VERHEIRATET;
		
		// Partner ebenfalls verheiraten
		if(!(partner.getFamilienstand() == Familienstand.VERHEIRATET)) {
			partner.heiraten(this);
		}
	}
	
	public void scheiden() {
		this.partner.familienstand = Familienstand.GESCHIEDEN;
		this.partner.partner = null;
		
		this.familienstand = Familienstand.GESCHIEDEN;
		this.partner = null;
	}
	
	public void zeigeInfo() {
		System.out.println("Name\t\t"+getName());
		System.out.println("Familienstand\t"+this.familienstand);
		
		if(this.familienstand == Familienstand.VERGEBEN || this.familienstand == Familienstand.VERHEIRATET) {
			System.out.println("Partner\t\t"+this.partner.getName());
		}
	}

	// Getter und Setter
	public String getNachname() {
		return nachname;
	}

	public String getVorname() {
		return vorname;
	}

	public char getGeschlecht() {
		return geschlecht;
	}

	public int getKoerpergroesseInCm() {
		return koerpergroesseInCm;
	}
	
	public Familienstand getFamilienstand() {
		return familienstand;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public void setGeschlecht(char geschlecht) {
		this.geschlecht = geschlecht;
	}

	public void setKoerpergroesseInCm(int koerpergroesseInCm) {
		this.koerpergroesseInCm = koerpergroesseInCm;
	}
}
