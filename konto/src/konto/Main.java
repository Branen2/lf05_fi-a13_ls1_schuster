package konto;

public class Main {
	public static void main(String[] args) {
		Konto account1 = new Konto("DE00", 11);
		Konto account2 = new Konto("DE99", 22);
		Besitzer myOwner = new Besitzer("Luka", "Schuster", account1, account2);
		
		//stand 0
		System.out.println("stand anfang");

		myOwner.gesamtGeld();
		myOwner.gesamtUebersicht();
		
		// test einzahlen
		account1.einzahlen(4);
		account2.einzahlen(7);
		
		System.out.println("Geld eingezahlt");
		System.out.println("Konto 1: " + account1.getKontostand());
		System.out.println("Konto 2: " + account2.getKontostand());

		//test abheben
		account1.abheben(1);
		account2.abheben(3);

		System.out.println("_______________________________");
		System.out.println("geld abgehoben");
		System.out.println("Konto 1: " + account1.getKontostand());
		System.out.println("Konto 2: " + account2.getKontostand());
		
		//test �berweisen
		account2.ueberweisen(account1, 2);
		
		System.out.println("_______________________________");
		System.out.println("geld von 2 auf 1 �berwiesen");
		System.out.println("Konto 1: " + account1.getKontostand());
		System.out.println("Konto 2: " + account2.getKontostand());
		
		//test zugeh�rigkeit
		myOwner.gesamtUebersicht();
		account2.setKontonummer(2);
		
		System.out.println("_______________________________");
		System.out.println("Kontonummer ge�ndert");
		
		myOwner.gesamtGeld();
		myOwner.gesamtUebersicht();
	}
}
