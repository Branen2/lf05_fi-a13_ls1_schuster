import java.util.Arrays;
public class a4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int[] array = new int[6];
		
		array[0] = 3;
		array[1] = 7;
		array[2] = 12;
		array[3] = 18;
		array[4] = 37;
		array[5] = 42;
		
		int key1 = 12;
		int key2 = 13;
		boolean val1 = contains1(array, key1);
		boolean val2 = contains2(array, key2);
		
		System.out.println("Ist die Zahl "+key1+" in der Ziehung enthalten? \n"+val1);
		System.out.println("Ist die Zahl "+key2+" in der Ziehung enthalten? \n"+val2);
		
	}

		public static boolean contains1(final int[] array, final int key1) {
	        return Arrays.stream(array).anyMatch(i -> i == key1);
	}
		public static boolean contains2(final int[] array, final int key2) {
	        return Arrays.stream(array).anyMatch(i -> i == key2);
	}

}
